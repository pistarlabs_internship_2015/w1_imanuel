package main
import(
	"encoding/json"
	"fmt"
	"net/http"
)

type Payload struct{
	Stuff Data
}

type Data struct{
	Fruit Fruits
	Veggies Vegetables
}

type Fruits map[string]int
type Vegetables map[string]int

func serveRest(w http.ResponseWriter, r *http.Request) {

	app_key := r.FormValue("app_key")
	//device_token := r.PostFormValue("device_token")

	
	//fmt.Fprintf(w,string(response))
	fmt.Printf("app_key: ",app_key)
}

func main() {
	http.HandleFunc("/",serveRest)
	http.ListenAndServe("localhost:1337",nil)
}
	
func getJsonResponse()([]byte,error) {
	fruits :=make(map[string]int)
	fruits["Apples"]=25
	fruits["Oranges"]=11

	vegetables :=make(map[string]int)
	vegetables["Carrots"]=21
	vegetables["Peppers"]=0

	d:=Data{fruits, vegetables}
	p:=Payload{d}

	return json.MarshalIndent(p, ""," ")

}
