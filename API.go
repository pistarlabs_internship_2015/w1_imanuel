//API FILE

package main

import(
	"encoding/json"
	"fmt"
	"net/http"
 	"github.com/iwanbk/gobeanstalk"
    "log"
    "time"
)
//DATA REQUEST
type DataReq map[string]interface{}

type Data struct{
	Req DataReq
}

type Payload struct{
	DataRequest Data
}
var isValid bool
var app_key=""
var device_token=""
var phone_number=""

func serveRest(w http.ResponseWriter, r *http.Request) {
	//VALIDATION BY SANTI//
	//...
	app_key = r.FormValue("app_key")
	device_token= r.FormValue("device_token")
	phone_number= r.FormValue("phone_number")
	
	//fmt.Fprintf(w,string(response))
	fmt.Fprintf(w,"app_key: %s, device_token: %s, phone_number: %s",app_key,device_token, phone_number)

	fmt.Printf(r.URL.Path)
	isValid:=true //ASUMPTION DATA IS VALID	

	if isValid==true{
		//CALL METHOD JSON RESPONSE
		response, err:=getJsonResponse()
		if err!=nil{
			panic(err)
		}
		//SHOW JSON RESULT IN WEB
		fmt.Fprintf(w, string(response))
		

		//CONNECTING TO MONGO DB & INSERTING DATA BY DWI//
		//....

		//WRITING TO BEANSTALKD BY IMANUEL//
		//CONCATE ALL DATA
		byt := []byte(app_key+"#####CONCAT#####"+device_token+"#####CONCAT#####"+phone_number)
	
		//CALL BEANSTALKD WRITER METHOD
		BeanstalkdWriter(byt)
		fmt.Printf("sukses")
	}
}

func main() {
	http.HandleFunc("/",serveRest)
	http.ListenAndServe("192.168.0.59:1300",nil)
}

func getJsonResponse()([]byte, error) {
	//INSERT DATA DUMMY TO MAP
	req:=make(map[string]interface{})
	req["app_key"]=app_key
    req["device_token"]=device_token
	req["phone_number"]=phone_number

    d:=Data{req}
    p:=Payload{d}

    //RETURN JSON RESPONSE
    return json.MarshalIndent(p,""," ")
}

func BeanstalkdWriter(data []byte) {
    //CONNECT TO BEANSTALKD SERVER
    conn, err := gobeanstalk.Dial("localhost:11300")
    if err != nil {
        log.Fatal(err)
    }
    //INSERT DATA
    id, err := conn.Put(data, 0, 2*time.Second, 3*time.Second)
    if err != nil {
        log.Fatal(err)
    }
    log.Printf("Job id %d inserted\n", id)
}